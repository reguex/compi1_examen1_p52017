//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_CHARS_H
#define EXAMENCOMPILADORES1_CHARS_H

namespace util {

    class Chars {
    public:
        static const int zero = 48;
        static const int one = 49;
        static const int underscore = 95;
        static const int slash = 47;
        static const int equal = 61;
        static const int comma = 44;
        static const int greaterThan = 62;
        static const int colon = 59;
        static const int leftBracket = 91;
        static const int rightBracket = 93;

        static const bool isWhiteSpace(int c) {
            auto c_char = static_cast<char>(c);
            return c_char == ' ' || c_char == '\n' || c_char == '\t';
        }

        static const bool isLetter(int c) {
            return (c >= 65 && c <= 90) || (c >= 97 && c <= 122);
        }
    };

}

#endif //EXAMENCOMPILADORES1_CHARS_H
