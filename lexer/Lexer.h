//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_LEXER_H
#define EXAMENCOMPILADORES1_LEXER_H


#include <fstream>
#include "consumer/Consumer.h"

namespace lexer {
    class Lexer {
        std::ifstream &input;

        consumer::Consumer *getAppropriateConsumer();

    public:
        explicit Lexer(std::ifstream &input);

        Token getNextToken();
    };
}


#endif //EXAMENCOMPILADORES1_LEXER_H
