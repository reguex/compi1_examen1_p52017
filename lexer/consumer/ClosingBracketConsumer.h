//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_CLOSINGBRACKETCONSUMER_H
#define EXAMENCOMPILADORES1_CLOSINGBRACKETCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumer {
        class ClosingBracketConsumer : public Consumer {
        public:
            ClosingBracketConsumer();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //EXAMENCOMPILADORES1_CLOSINGBRACKETCONSUMER_H
