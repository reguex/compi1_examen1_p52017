//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_WHITESPACECONSUMER_H
#define EXAMENCOMPILADORES1_WHITESPACECONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumer {
        class WhiteSpaceConsumer : public Consumer {
        public:
            WhiteSpaceConsumer();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //EXAMENCOMPILADORES1_WHITESPACECONSUMER_H
