//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_OPENBRACKETCONSUMER_H
#define EXAMENCOMPILADORES1_OPENBRACKETCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumer {
        class OpenBracketConsumer : public Consumer {
        public:
            OpenBracketConsumer();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //EXAMENCOMPILADORES1_OPENBRACKETCONSUMER_H
