//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_IDCONSUMER_H
#define EXAMENCOMPILADORES1_IDCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumer {
        class IdConsumer : public Consumer {
        protected:
        public:
            explicit IdConsumer();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //EXAMENCOMPILADORES1_IDCONSUMER_H
