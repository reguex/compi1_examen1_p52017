//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_NUMBERCONSUMER_H
#define EXAMENCOMPILADORES1_NUMBERCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumer {
        class NumberConsumer : public Consumer {
        public:
            NumberConsumer();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //EXAMENCOMPILADORES1_NUMBERCONSUMER_H
