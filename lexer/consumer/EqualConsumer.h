//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_EQUALCONSUMER_H
#define EXAMENCOMPILADORES1_EQUALCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumer {
        class EqualConsumer : public Consumer {
        public:
            EqualConsumer();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //EXAMENCOMPILADORES1_EQUALCONSUMER_H
