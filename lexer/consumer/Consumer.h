//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_CONSUMER_H
#define EXAMENCOMPILADORES1_CONSUMER_H


#include <fstream>
#include "../Token.h"

namespace lexer {
    namespace consumer {

        class Consumer {
            int initial;
        public:
            explicit Consumer(int initial);

            Token process(std::ifstream &source);

        protected:
            virtual int delta(int current, int value) = 0;
            virtual bool isFinal(int state) = 0;
            virtual std::string getStateTokenType(int state) = 0;

        public:
            virtual ~Consumer();

        protected:
            void raiseUndefinedTokenTypeForState(int state);
        };

    }
}


#endif //EXAMENCOMPILADORES1_CONSUMER_H
