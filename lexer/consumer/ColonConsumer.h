//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_COLONCONSUMER_H
#define EXAMENCOMPILADORES1_COLONCONSUMER_H


#include "Consumer.h"

namespace lexer {
    namespace consumer {
        class ColonConsumer : public Consumer {
        public:
            ColonConsumer();

        protected:
            int delta(int current, int value) override;

            bool isFinal(int state) override;

            std::string getStateTokenType(int state) override;
        };
    }
}


#endif //EXAMENCOMPILADORES1_COLONCONSUMER_H
