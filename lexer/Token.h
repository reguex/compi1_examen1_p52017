//
// Created by Doninelli on 11/6/17.
//

#ifndef EXAMENCOMPILADORES1_TOKEN_H
#define EXAMENCOMPILADORES1_TOKEN_H


#include <string>
#include <ostream>

namespace lexer {
    class Token {
        std::string type;
        std::string lexeme;

    public:
        Token(const std::string &type, const std::string &lexeme) : type(type), lexeme(lexeme) {}

        const std::string &getType() const {
            return type;
        }

        const std::string &getLexeme() const {
            return lexeme;
        }

        friend std::ostream &operator<<(std::ostream &os, const Token &token) {
            os << "type: " << token.type << " lexeme: " << token.lexeme;
            return os;
        }
    };
}


#endif //EXAMENCOMPILADORES1_TOKEN_H
