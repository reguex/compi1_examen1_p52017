//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/OpenBracketConsumer.h"
#include "../../../utils/Chars.h"

lexer::consumer::OpenBracketConsumer::OpenBracketConsumer() : Consumer(1) {}

int lexer::consumer::OpenBracketConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::leftBracket) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumer::OpenBracketConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumer::OpenBracketConsumer::getStateTokenType(int state) {
    if (state == 2) {
        return "left_bracket";
    }

    raiseUndefinedTokenTypeForState(state);
}
