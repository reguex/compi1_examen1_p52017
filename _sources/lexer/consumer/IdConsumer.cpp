//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/IdConsumer.h"
#include "../../../utils/Chars.h"

int lexer::consumer::IdConsumer::delta(int current, int value) {
    if (current == 1) {
        if (util::Chars::isLetter(value) || value == util::Chars::underscore) {
            return 2;
        }
    } else if (current == 2) {
        if (util::Chars::isLetter(value) || isnumber(value) || value == util::Chars::underscore) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumer::IdConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumer::IdConsumer::getStateTokenType(int state) {
    if (state == 2) {
        return "id";
    }

    raiseUndefinedTokenTypeForState(2);
}

lexer::consumer::IdConsumer::IdConsumer() : Consumer(1) {}
