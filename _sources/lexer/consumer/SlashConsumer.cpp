//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/SlashConsumer.h"
#include "../../../utils/Chars.h"

lexer::consumer::SlashConsumer::SlashConsumer() : Consumer(1) {}

int lexer::consumer::SlashConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::slash) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::slash) {
            return 3;
        }
    } else if (current == 3) {
        if (value != '\n') {
            return 3;
        }
    }

    return -1;
}

bool lexer::consumer::SlashConsumer::isFinal(int state) {
    return state == 3;
}

std::string lexer::consumer::SlashConsumer::getStateTokenType(int state) {
    if (state == 3) {
        return "comment";
    }

    raiseUndefinedTokenTypeForState(state);
}
