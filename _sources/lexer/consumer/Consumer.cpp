//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/Consumer.h"

lexer::consumer::Consumer::Consumer(int initial) : initial(initial) {}

lexer::Token lexer::consumer::Consumer::process(std::ifstream &source) {
    std::string lexeme;

    auto current = initial;
    while (!source.eof() && source.peek() != -1) {
        auto next = delta(current, source.peek());
        if (next < 0) {
            break;
        }

        current = next;
        lexeme += static_cast<char>(source.get());
    }

    if (!isFinal(current)) {
        throw std::invalid_argument("invalid input");
    }

    return Token(lexeme, getStateTokenType(current));
}

void lexer::consumer::Consumer::raiseUndefinedTokenTypeForState(int state) {
    std::string error;
    error += std::to_string(state);
    throw std::invalid_argument(error);
}

lexer::consumer::Consumer::~Consumer() = default;