//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/CommaConsumer.h"
#include "../../../utils/Chars.h"

lexer::consumer::CommaConsumer::CommaConsumer() : Consumer(1) {}

int lexer::consumer::CommaConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::comma) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumer::CommaConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumer::CommaConsumer::getStateTokenType(int state) {
    if (state == 2) {
        return "comma";
    }

    raiseUndefinedTokenTypeForState(state);
}
