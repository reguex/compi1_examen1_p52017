//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/NumberConsumer.h"

lexer::consumer::NumberConsumer::NumberConsumer() : Consumer(1) {}

int lexer::consumer::NumberConsumer::delta(int current, int value) {
    if (current == 1) {
        if (isnumber(value)) {
            return 2;
        }
    } else if (current == 2) {
        if (isnumber(value)) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumer::NumberConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumer::NumberConsumer::getStateTokenType(int state) {
    if (state == 2) {
        return "number";
    }

    raiseUndefinedTokenTypeForState(state);
}
