//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/WhiteSpaceConsumer.h"
#include "../../../utils/Chars.h"

lexer::consumer::WhiteSpaceConsumer::WhiteSpaceConsumer() : Consumer(1) {}

int lexer::consumer::WhiteSpaceConsumer::delta(int current, int value) {
    if (current == 1) {
        if (util::Chars::isWhiteSpace(value)) {
            return 2;
        }
    } else if (current == 2) {
        if (util::Chars::isWhiteSpace(value)) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumer::WhiteSpaceConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumer::WhiteSpaceConsumer::getStateTokenType(int state) {
    if (state == 2) {
        return "whitespace";
    }

    raiseUndefinedTokenTypeForState(state);
}
