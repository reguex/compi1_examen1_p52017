//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/ClosingBracketConsumer.h"
#include "../../../utils/Chars.h"

lexer::consumer::ClosingBracketConsumer::ClosingBracketConsumer() : Consumer(1) {}


int lexer::consumer::ClosingBracketConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::rightBracket) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumer::ClosingBracketConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumer::ClosingBracketConsumer::getStateTokenType(int state) {
    if (state == 2) {
        return "right_bracket";
    }

    raiseUndefinedTokenTypeForState(state);
}
