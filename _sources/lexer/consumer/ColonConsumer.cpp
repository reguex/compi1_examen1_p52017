//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/ColonConsumer.h"
#include "../../../utils/Chars.h"

lexer::consumer::ColonConsumer::ColonConsumer() : Consumer(1) {}

int lexer::consumer::ColonConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::colon) {
            return 2;
        }
    }

    return -1;
}

bool lexer::consumer::ColonConsumer::isFinal(int state) {
    return state == 2;
}

std::string lexer::consumer::ColonConsumer::getStateTokenType(int state) {
    if (state == 2) {
        return "colon";
    }

    raiseUndefinedTokenTypeForState(state);
}
