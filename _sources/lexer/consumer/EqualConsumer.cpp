//
// Created by Doninelli on 11/6/17.
//

#include "../../../lexer/consumer/EqualConsumer.h"
#include "../../../utils/Chars.h"

lexer::consumer::EqualConsumer::EqualConsumer() : Consumer(1) {}

int lexer::consumer::EqualConsumer::delta(int current, int value) {
    if (current == 1) {
        if (value == util::Chars::equal) {
            return 2;
        }
    } else if (current == 2) {
        if (value == util::Chars::greaterThan) {
            return 3;
        }
    }

    return -1;
}

bool lexer::consumer::EqualConsumer::isFinal(int state) {
    return state == 3;
}

std::string lexer::consumer::EqualConsumer::getStateTokenType(int state) {
    if (state == 3) {
        return "arrow";
    }

    raiseUndefinedTokenTypeForState(state);
}
