//
// Created by Doninelli on 11/6/17.
//

#include <iostream>
#include "../../lexer/Lexer.h"
#include "../../utils/Chars.h"
#include "../../lexer/consumer/SlashConsumer.h"
#include "../../lexer/consumer/WhiteSpaceConsumer.h"
#include "../../lexer/consumer/NumberConsumer.h"
#include "../../lexer/consumer/IdConsumer.h"
#include "../../lexer/consumer/EqualConsumer.h"
#include "../../lexer/consumer/OpenBracketConsumer.h"
#include "../../lexer/consumer/ClosingBracketConsumer.h"
#include "../../lexer/consumer/ColonConsumer.h"
#include "../../lexer/consumer/CommaConsumer.h"

lexer::Lexer::Lexer(std::ifstream &input) : input(input) {}

lexer::Token lexer::Lexer::getNextToken() {
    if (input.peek() == -1 || input.eof()) {
        return Token("eof", "");
    }

    if (util::Chars::isWhiteSpace(input.peek())) {
        consumer::WhiteSpaceConsumer().process(input);
        return getNextToken();
    }

    if (input.peek() == util::Chars::slash) {
        consumer::SlashConsumer().process(input);
        return getNextToken();
    }

    consumer::Consumer *consumer = getAppropriateConsumer();
    Token token = consumer->process(input);
    delete consumer;

    return token;
}

lexer::consumer::Consumer *lexer::Lexer::getAppropriateConsumer() {
    auto currentChar = input.peek();

    if (isnumber(currentChar)) {
        return new consumer::NumberConsumer();
    }

    if (currentChar == util::Chars::underscore || util::Chars::isLetter(currentChar)) {
        return new consumer::IdConsumer();
    }

    if (currentChar == util::Chars::equal) {
        return new consumer::EqualConsumer();
    }

    if (currentChar == util::Chars::leftBracket) {
        return new consumer::OpenBracketConsumer();
    }

    if (currentChar == util::Chars::rightBracket) {
        return new consumer::ClosingBracketConsumer();
    }

    if (currentChar == util::Chars::colon) {
        return new consumer::ColonConsumer();
    }

    if (currentChar == util::Chars::comma) {
        return new consumer::CommaConsumer();
    }

    std::string error;
    error += static_cast<char>(currentChar);
    throw std::invalid_argument(error);
}