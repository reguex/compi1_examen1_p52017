#include <iostream>
#include <fstream>
#include "lexer/Lexer.h"

int main() {
    std::string file;
    std::cout << "File: ";
    std::cin >> file;

    std::ifstream ifs;

    ifs.open(file);
    lexer::Lexer lexer(ifs);
    std::shared_ptr<lexer::Token> token;

    do {
        token = std::make_shared<lexer::Token>(lexer.getNextToken());
        std::cout << token->getType() << " -> " << token->getLexeme() << std::endl;
    } while (token->getType() != "eof");

    ifs.close();
}