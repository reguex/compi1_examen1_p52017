cmake_minimum_required(VERSION 3.8)
project(ExamenCompiladores1)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp _sources/lexer/consumer/Consumer.cpp lexer/consumer/Consumer.h lexer/Token.h _sources/lexer/consumer/IdConsumer.cpp lexer/consumer/IdConsumer.h utils/Chars.h _sources/lexer/consumer/WhiteSpaceConsumer.cpp lexer/consumer/WhiteSpaceConsumer.h _sources/lexer/consumer/CommaConsumer.cpp lexer/consumer/CommaConsumer.h _sources/lexer/consumer/EqualConsumer.cpp lexer/consumer/EqualConsumer.h _sources/lexer/consumer/ColonConsumer.cpp lexer/consumer/ColonConsumer.h _sources/lexer/consumer/OpenBracketConsumer.cpp lexer/consumer/OpenBracketConsumer.h _sources/lexer/consumer/ClosingBracketConsumer.cpp lexer/consumer/ClosingBracketConsumer.h _sources/lexer/consumer/SlashConsumer.cpp lexer/consumer/SlashConsumer.h _sources/lexer/consumer/NumberConsumer.cpp lexer/consumer/NumberConsumer.h _sources/lexer/Lexer.cpp lexer/Lexer.h _sources/parser/Parser.cpp parser/Parser.h)
add_executable(ExamenCompiladores1 ${SOURCE_FILES})